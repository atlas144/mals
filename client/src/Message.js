export default class Message {
  payload;

  constructor(payload) {
    this.payload = payload;
  }
}
