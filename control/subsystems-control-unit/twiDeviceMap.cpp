#include "twiDeviceMap.h"

TwiDeviceMap::TwiDeviceMap() {
    size = 1;
    occupied = 0;
    keys = new uint8_t[size];
    values = new TwiDevice*[size];
}

TwiDevice* TwiDeviceMap::get(uint8_t key) {
    for (uint8_t i = 0; i < occupied; i++) {
        if (key == keys[i]) {
            return values[i];
        }
    }

    return NULL;
}

void TwiDeviceMap::set(uint8_t key, TwiDevice* value) {
    for (uint8_t i = 0; i < occupied; i++) {
        if (key == keys[i]) {
            values[i] = value;
            return;
        }
    }

    if (occupied < size) {
        keys[occupied] = key;
        values[occupied] = value;
        occupied++;
    } else {
        uint8_t* newKeys = new uint8_t[size + 1];
        TwiDevice** newValues = new TwiDevice*[size + 1];

        for (uint8_t i = 0; i < size; i++) {
            newKeys[i] = keys[i];
            newValues[i] = values[i];
        }

        size++;

        newKeys[size] = key;
        newValues[size] = value;

        occupied++;

        delete [] keys;
        delete [] values;

        keys = newKeys;
        values = newValues;
    }
}