#ifndef SERIALIZABLE_FLOAT_H
#define SERIALIZABLE_FLOAT_H

union SerializableFloat {
  float number;
  uint8_t binary[4];
};

#endif
