#ifndef TWI_DEVICE_MAP_H
#define TWI_DEVICE_MAP_H

#include <Arduino.h>
#include "twiDevice.h"

class TwiDeviceMap {
	private:
		uint8_t size;
		uint8_t occupied;
		uint8_t* keys;
		TwiDevice** values;

	public:
		TwiDeviceMap();
		TwiDevice* get(uint8_t key);
		void set(uint8_t key, TwiDevice* value);
};

#endif
