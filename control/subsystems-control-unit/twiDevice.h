#ifndef TWI_DEVICE_H
#define TWI_DEVICE_H

struct TwiDevice {
  uint8_t twiAddress;
  uint8_t dataBufferSize;
  uint8_t* data;

  uint8_t srscPacketType;
  uint8_t srscPacketSize;
  bool srscPacketIsCritical;

  uint32_t cycleDuration;
};

#endif
