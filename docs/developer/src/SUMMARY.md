# Summary

- [Main control unit](./mcu/index.md)
  - [Task modules](./mcu/task-modules/index.md)
    - [Manual control](./mcu/task-modules/manual-control.md)
    - [WebSocket](./mcu/task-modules/websocket.md)
    - [Monitoring](./mcu/task-modules/monitoring.md)
    - [Test](mcu/task-modules/test.md)
