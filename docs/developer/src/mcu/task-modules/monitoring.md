# Monitoring

The *Monitoring* module collects sensory data from the entire system, processes it and converts it into a form suitable for **display**. Client monitoring modules should primarily display data obtained from this module (instead of processing the raw data themselves).

## Working principle

The module subscribes to data from **all sensors** of the vehicle and stores it. For each *topic* subscribed this way, it keeps the sum of the received values (the number of samples is `n`). If the addition of a new value causes `n` to exceed a `threshold` (specified for each *topic*), the averaged value is sent to the corresponding *topic*.

Between receiving and sending, the *topic* is transformed - the input value starts with `/data/raw/sensory` and the output value starts with `/data/display/monitoring`.

## MIB topics

### Subscribed

- `/data/raw/sensory/*` - raw data from all sensors

### Published

- `/data/display/monitoring/*` - processed data for monitoring (for every subscribed *topic*)
