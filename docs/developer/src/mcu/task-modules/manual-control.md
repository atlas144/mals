# Manual control

The purpose of the *Manual control* module is to provide the ability to control the vehicle by a **human operator**. It should serve as a complement for automation control modules (e.g. to solve edge cases).