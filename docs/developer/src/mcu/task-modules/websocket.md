# WebSocket

The *WebSocket* module is primarily used to connect *user clients*, but can be considered as a **general interface** for connecting modules to the central [MIB](https://codeberg.org/atlas144/mals-intermodule-broker) broker. This is achieved by the module acting as a **translator** between *WebSocket* and *MIB* messages.

## Message

Two types of messages are currently supported: `SUBSCRIBE` and `PUBLISH`. They are equivalent to the corresponding functions from the [MIB](https://codeberg.org/atlas144/mals-intermodule-broker) library.

If a message that does not match the following format or does not contain one of the defined types is received, it is logged and then discarded.

### General message structure

```json
{
    "type": "TYPE_OF_THE_MESSAGE",
    "payload": {
        // depends on the type
    }
}
```

### SUBSCRIBE

Receiving a `SUBSCRIBE` message invokes the `subscribe` method on the *broker* ([MIB](https://codeberg.org/atlas144/mals-intermodule-broker)). The *topic* to be subscribed is located in the `topic` field of the message `payload` section.

#### SUBSCRIBE message structure

```json
{
    "type": "SUBSCRIBE",
    "payload": {
        "topic": "topic/to/be/subscribed"
    }
}
```

### PUBLISH

The `PUBLISH` message invokes the `publish` method on the *broker* (MIB). The `payload` section contains the *topic* of the *MIB message* (`topic`), its *content* (`payload`), and its *priority* (`priority`).

The value of the `priority` field must match the value in the *MIB* `Priority` enum. That is:

- `UNIMPORTANT`
- `NORMAL`
- `IMPORTANT`
- `CRITICAL`

#### PUBLISH message structure

```json
{
    "type": "PUBLISH",
    "payload": {
        "topic": "topic/of/the/mib/message",
        "payload": "content of the message",
        "priority": "NORMAL"
    }
}
```
