# Test

The *Test* module is used to demonstrate the communication between the *MCU* and the *client*. Its only purpose is to receive messages from the client *Test* module and combine them into a response.

## MIB topics

### Subscribed

- `/data/raw/test/input` - value of the client *Test* module `input` field
- `/data/raw/test/select` - value of the client Test module `select` field

### Published

- `/data/display/test/label` - combination of the values of the `input` and `select` fields of the client *Test* module (the values are combined after they are received from the client)
